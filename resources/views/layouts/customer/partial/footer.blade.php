<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h4>Ayo Gadai</h4>
            <p>Ayo gadai merupakan platform yang membantu masyarakat dalam memperoleh dana lebih cepat dan menguntungkan</p>

            <div class="social-links">
              <a href="https://id-id.facebook.com/login/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/login?lang=id" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.instagram.com/accounts/login/?hl=id" class="instagram"><i class="fa fa-instagram"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Alamat</h4>
            <ul>
              <li><p>Jl. Sisingamangaraja No. 41, Medan</p>
              <li><p>Kode Pos: 12635<p></li>
              <li><p>Phone: +62 8 5589 5548 55</p></li>
              <li><p>FX: 542136</p></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Kategori Barang Gadai</h4>
            <ul>
              <li><a href="/gadai-barang/Handphone">Handphone</a></li>
              <li><a href="/gadai-barang/Laptop">Laptop</a></li>
              <li><a href="/gadai-barang/Perhiasan">Perhiasan</a></li>
              <li><a href="/gadai-barang/Elektronik">Elektronik</a></li>
              <li><a href="/gadai-barang/Kendaraan">Kendaraan</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>SiteMap</h4>
            <ul>
              <li><a href="/">Beranda</a></li>
              <li><a href="/kategori_barang">Gadai</a></li>
              <li><a href="/profile/{{auth()->user()->id}}">Profil</a></li>
              <li><a href="/kontak-kami">Kontak Kami</a></li>
            </ul>   
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="hr"> 
        <hr>
      </div>
    </div>

    <div class="container">
      <div class="copyright"><strong>@ <?php echo date("Y"); ?> Ayo Gadai | Aplikasi Web Pegadaian</strong> 
      </div>
    </div>
  </footer>