<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.hasthemes.com/autima-preview/autima/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 09:23:56 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ayo Gadai</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{url('assets/img/favicon.ico')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

     <!-- CSS 
    ========================= -->
     <!--bootstrap min css-->
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <!--owl carousel min css-->
    <link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
    <!--slick min css-->
    <link rel="stylesheet" href="{{url('assets/css/slick.css')}}">
    <!--magnific popup min css-->
    <link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">
    <!--font awesome css-->
    <link rel="stylesheet" href="{{url('assets/css/font.awesome.css')}}">
    <!--ionicons min css-->
    <link rel="stylesheet" href="{{url('assets/css/ionicons.min.css')}}">
    <!--animate css-->
    <link rel="stylesheet" href="{{url('assets/css/animate.css')}}">
    <!--jquery ui min css-->
    <link rel="stylesheet" href="{{url('assets/css/jquery-ui.min.css')}}">
    <!--slinky menu css-->
    <link rel="stylesheet" href="{{url('assets/css/slinky.menu.css')}}">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{url('assets/css/plugins.css')}}">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
    
    <!--modernizr min js here-->
    <script src="{{url('assets/js/vendor/modernizr-3.7.1.min.js')}}"></script>

</head>

<body>
    <!-- Bagian Header -->
    @include('layouts/customer/partial/header')
    <!-- End Bagian Header -->


    <!-- Bagian Menu -->
    @include('layouts/customer/partial/menu')
    <!-- End Bagian Menu -->

    @yield('content')

    <!--gadai  area end-->
    <!--call to action end-->
    <!--footer area start-->
    @include('layouts/customer/partial/footer')
    <!--footer area end-->


 <!-- JS
============================================ -->
<!--jquery min js-->
<script src="{{url('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>
<!--popper min js-->
<script src="{{url('assets/js/popper.js')}}"></script>
<!--bootstrap min js-->
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<!--owl carousel min js-->
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<!--slick min js-->
<script src="{{url('assets/js/slick.min.js')}}"></script>
<!--magnific popup min js-->
<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
<!--jquery countdown min js-->
<script src="{{url('assets/js/jquery.countdown.js')}}"></script>
<!--jquery ui min js-->
<script src="{{url('assets/js/jquery.ui.js')}}"></script>
<!--jquery elevatezoom min js-->
<script src="{{url('assets/js/jquery.elevatezoom.js')}}"></script>
<!--isotope packaged min js-->
<script src="{{url('assets/js/isotope.pkgd.min.js')}}"></script>
<!--slinky menu js-->
<script src="{{url('assets/js/slinky.menu.js')}}"></script>
<!-- Plugins JS -->
<script src="{{url('assets/js/plugins.js')}}"></script>

<!-- Main JS -->
<script src="{{url('assets/js/main.js')}}"></script>



</body>


<!-- Mirrored from demo.hasthemes.com/autima-preview/autima/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 09:23:57 GMT -->
</html>